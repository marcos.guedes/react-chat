import React from "react";
import GoogleSignin from "../images/btn_google_signin_dark_pressed_web.png";
import { auth } from "../firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import { GoogleAuthProvider, signInWithRedirect } from "firebase/auth";
import { MaterialSymbol } from 'react-material-symbols';
import 'react-material-symbols/dist/rounded.css';

const SignIn = () => {
  const [user] = useAuthState(auth);

  const googleSignIn = () => {
    const provider = new GoogleAuthProvider();
    signInWithRedirect(auth, provider);
  };

  const signOut = () => {
    auth.signOut();
  };

  return (
    <React.Fragment>
      {user ? (
        <button onClick={signOut} className="btn sign-out" type="button">
          <MaterialSymbol icon="logout" /> Sign Out
        </button>
      ) : (
        <button className="signin-btn">
          <img
            onClick={googleSignIn}
            src={GoogleSignin}
            alt="sign in with google"
            type="button"
          />
        </button>
      )}
    </React.Fragment>
  );
}

export default SignIn;