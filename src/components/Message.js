import React, { useState } from "react";
import { auth, db } from "../firebase";
import { doc, deleteDoc, updateDoc } from "firebase/firestore";
import { useAuthState } from "react-firebase-hooks/auth";
import moment from 'moment';
import { MaterialSymbol } from 'react-material-symbols';
import 'react-material-symbols/dist/rounded.css';

const Message = ({message}) => {
  const [user] = useAuthState(auth);
  const messageDate = message.createdAt ? message.createdAt.toDate() : '';
  const [editMode, setEditMode] = useState(false);
  
  return (
    <div className={`chat-bubble ${message.uid === user.uid ? "right" : ""} ${message.uid === user.uid && editMode ? "edit-mode" : ""}`}>
      <picture className="chat-bubble--avatar">
        <img src={message.avatar} alt="avatar" referrerPolicy="no-referrer" />
      </picture>

      <div className="chat-bubble--message">
        <div className="user-name">{message.name}</div>
        <div className="date">{moment(messageDate).format('MMM Do YYYY, h:mm A')}</div>
        {editMode ? <EditMessageForm editMode={editMode} setEditMode={setEditMode} message={message} /> : <div className="user-message">{message.text}</div>}
        

        {message.uid === user.uid && <div className="actions">
          {message.edited && <span className="edited">edited</span>}
          {!editMode && <EditMessage setEditMode={setEditMode} />}
          <DeleteMessage messageID={message.id} />
        </div>}
      </div>
    </div>
  ); 
};

const DeleteMessage = ({messageID}) => {
  const handleDelete = async (id) => {
    await deleteDoc(doc(db, "messages", id));
  }

  return (
    <div className="action-btn delete-btn" onClick={() => handleDelete(messageID)}>
      <MaterialSymbol icon="delete" />
    </div>
  )
};

const EditMessage = ({setEditMode}) => {
  return (
    <div className="action-btn edit-btn" onClick={() => setEditMode(true)}>
      <MaterialSymbol icon="edit" />
    </div>
  )
};

const EditMessageForm = ({editMode, setEditMode, message}) => {

  const [updatedMessage, setUpdatedMessage] = useState(message.text);

  const handleSubmit = async (event, id) => {
    event.preventDefault();

    if (updatedMessage.trim() === "") {
      alert("Enter valid message");
      return;
    }

    const messageRef = doc(db, "messages", id);
    await updateDoc(messageRef, {
      text: updatedMessage,
      edited: true
    });

    setEditMode(false);
  }

  return (
    <form className="edit-message-form" onSubmit={(event) => handleSubmit(event, message.id)}>
      <div 
        className="btn cancel-btn"
        onClick={() => setEditMode(false)}>
        <MaterialSymbol icon="cancel" />
      </div>
      <input 
        type="text"
        value={updatedMessage}
        onChange={(e) => setUpdatedMessage(e.target.value)}
      ></input>
      <button className="btn btn-primary edit-btn">
        <MaterialSymbol icon="edit" />
      </button>
      
    </form>
  )
}

export default Message;