import React from "react";
import SignIn from './SignIn';

const Welcome = () => {

  return (
    <main className="welcome">
      <h2>Welcome to React Chat.</h2>
      <p>Sign in with Google to chat with with your fellow React Developers.</p>
      <SignIn />
    </main>
  );

}

export default Welcome;