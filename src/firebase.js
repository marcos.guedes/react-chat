// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDGFpZOYM8Kul3mtuVLo7UCS3ahV_BiL8A",
  authDomain: "react-chat-81a3b.firebaseapp.com",
  projectId: "react-chat-81a3b",
  storageBucket: "react-chat-81a3b.appspot.com",
  messagingSenderId: "658298121511",
  appId: "1:658298121511:web:86b4332d04fb807e808d02"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);